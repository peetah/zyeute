# Zyeute: a minimalist monitoring tool

## What is Zyeute ?

Zyeute is a monitoring CLI tool running user defined tests for user defined
contexts.

Each test failure results in a warning message.  
Each test success following at least one failure of the same test results in a
recovering message.

Zyeute output can then be sent to whoever is concerned using your prefered means.

Zyeute can be run as a standalone process, but it's certainly more useful when
delegated to a job scheduler such as cron.

Zyeute was first developed to check files and web servers availability. It has
since been freed from these constraints and its usage is only limited by users
imagination.

Please share your own use cases for this tool.

## What are Zyeute requirements ?

Zyeute depends on the following tools:

* [bash](https://www.gnu.org/software/bash/), but it would be nice to get rid of all bashisms
* find
* grep
* sed
* [jq](https://github.com/stedolan/jq) >= 1.5

## How to install Zyeute ?

1. Copy the file [`zyeute`](zyeute) in one of the directories included in your PATH
environment variable

2. Verify that `zyeute` is executable.

3. Create a configuration file (default: $HOME/.config/zyeute/config.json)

4. Put your tests file tree in the directory of your configuration file

5. You're good to go !

## How to configure Zyeute ?

The Zyeute configuration is stored using the JSON open standard format.

What follows describes the data and their structure as found in a generic Zyeute
configuration.

For a more pragmatic example, check the [`config/`](config) directory of this
repository: it contains the configuration used to validate Zyeute expected
behavior.

### TL;DR

Zyeute configuration is composed of nested 'Testee' objects.


'Testee' objects are composed of:
* one optional 'Options' object named '\_options'
* none or several 'Context' objects whose name always starts with an underscore (\_)


'Context' objects are composed of:
* one optional 'Options' object named '\_options'
* one or several 'Testee' properties whose value can be:
  * null
  * an array of paths to executables
  * a 'Testee' object


The root of the configuration file is a Testee object:

---
```json
{
  "_options":{
    "optionName1": "optionValue1",
    "optionName2": "optionValue2"
    …
  },
  "_contextName1":{
    "_options":{
      "optionName3": "optionValue3",
      "optionName4": "optionValue4"
       …
    },
    "testeeName1": null
    "testeeName2": ["array","of","paths","to","executables"]
    "testeeName3": {
       __Another_Testee_object__
    },
    …
  },
  …
}
```
---


The '\_options" objects accept various properties depending on the nature of
their parent:


---
| Parent nature | Option name         | Option type | Default value | Description
|---------------|---------------------|:-----------:|:-------------:|------------
| Root          | debug               | boolean     | false         | enable debug messages
|               | allowMultipleAlerts | boolean     | false         | repeat warning messages
|
| Context       | tests               | paths array | []            | set of tests to run
|               | fatal               | boolean     | false         | skip remaining tests for this context
|               | \<any name\>          | \<any type\>  | null          | user defined options
|
| Testee        | tests               | paths array | []            | set of tests to run
|               | skip                | boolean     | false         | skip test for this testee for current context
|               | \<any name\>          | \<any type\>  | null          | user defined options

---


### Zyeute root options

The most basic and totally useless configuration defines only an '\_options'
object which set the options impacting the Zyeute workflow:

---
```json
{
  "_options": {
    "debug": false,
    "allowMultipleAlerts": false
  }
}
```
---

The available Zyeute options are:

* debug \[boolean, false\] : enable debug messages

* allowMultipleAlerts \[boolean, false\] : repeat warning message for each
consecutive failure of the same test

If one of these options is not defined in the '\_options' object, or if the
'\_options' object itself is missing, the default values are used.


### Zyeute testing playground

Tests are run in a given context for specific testees.

In the Zyeute configuration, contexts are defined as Context objects whose names
start with an underscore (\_).  
They can take any name but '\_options' which is reserved for Zyeute usage.

---
```json
{
  "_options": {
    "debug": false,
    "allowMultipleAlert": false
  },
  "_context1": {
    "_options": {
      "tests": ["testA","/usr/bin/testB"],
      "fatal": true,
      "myContextOption": "whateverValue"
    },
    "testee1": null,
    "testee2": ["test1","/usr/bin/test2"],
  },
}
```
---


A context named '\_context1' has been added next to the root '\_options' object.
It is composed of 3 elements: a context '\_options' object and two testees.

The available context options are:

* tests \[array, \[\]\]: an array of paths to executable files used as tests;
relative paths are based on the 'tests' directory located in the configuration
file directory.

* fatal \[boolean, false\]: skip all subsequent tests applied to a testee if one
of the test run in this context fails

In addition to those two predefined options, any other option can be defined if
it is useful to or required by one or several of the tests to be run. This may 
be the case of the 'myContextOption' option.

Testee properties are the subjects of the tests to be run. If the tests defined
in the context '\_options' object are sufficient, Testee properties value must
be set to null.
Otherwise, each Testee property can add more tests to run by having their value
set to an array of paths to excutable files.

As a result of the above configuration, a testee is first submitted to every
test defined in the 'tests' context option. When applicable, it is then
submitted to the specific tests defined in its value array.

Using the appropriate command line arguments, this is what Zyeute runs for you:

```sh
$HOME/config/tests/testA context1=testee1 context1_fatal=true context1_myContextOption=whateverValue
/usr/bin/testB context1=testee1 context1_fatal=true context1_myContextOption=whateverValue
$HOME/config/tests/testA context1=testee2 context1_fatal=true context1_myContextOption=whateverValue
/usr/bin/testB context1=testee2 context1_fatal=true context1_myContextOption=whateverValue
$HOME/config/tests/test1 context1=testee2 context1_fatal=true context1_myContextOption=whateverValue
/usr/bin/test2 context1=testee2 context1_fatal=true context1_myContextOption=whateverValue
```

Since the 'fatal' option is set to true for '\_context1', if one test fails for
a testee, all other tests for this testee will be skipped.


### Zyeute more complex testing playground

Now, consider a third testee which requires something a little bit more advanced:

---
```json
{
  "_options": {
    "debug": false,
    "allowMultipleAlert": false
  },
  "_context1": {
    "_options": {
      "tests": ["testA","/usr/bin/testB"],
      "fatal": true,
      "myContextOption": "whateverValue"
    },
    "testee1": null,
    "testee2": ["test1","/usr/bin/test2"],
    "testee3": {
      "_options": {
        "skip": true
      },
      "_subContext": {
        "subTestee1": null,
        "subTestee2":{
          "_options":{
            "my_option": "my_option_value"
          }
        }
      }
    }
  },
}
```
---


Instead of a null value or an array, this testee3 is an object composed of two
others: a testee '\_options' object, and a '\_subContext' object.

The available testee options are:

* tests \[array, null\]: as described for the 'tests' context option, an array
of paths to executable files used as tests; it's the equivalent of the testee
value when other options need to be set

* skip \[boolean, false\]: skip the tests defined for the current context; this
can be useful when a test defined at a context level is known to always
fail for a given testee.

As described for the context options, any other option can be defined if useful
or required, as this is the case for the 'my_option' option of subTestee2.

In the above configuration, the skip option indicates that testee3 does not need
to be directly submitted to tests defined in '\_context1' object. But it seems
to require that elements somehow related to testee3 need to be tested in a
context called '\_subContext'.

This may requires a more pragmatic example.

To verify that a web server is actually serving the expected web page via an
encrypted connection, several elements need to be checked, each of them being a
context for Zyeute:

* IPs context: web server host connectivity
* Domain names context: DNS lookup
* Protocols context:
  * secured protocol availability
  * domain name matching certificate
  * certificate expiration date
* URLs context: 
  * content availability
  * content integrity

Each of these context is usually relevant only if the previous has been validated.
If the IPs context corresponds to our '\_context1', then Domain names context
corresponds to '\_subContext'. URLs context will then be nested within Protocols
context which in turn will be nested within Domain names context.

Contexts can be nested without limits.

## Zyeute tests

Are you still reading ? It's pretty impressive that you do !

Then, there is a slim chance you noticed that the above subTestee1 and subTestee2
are not submitted to any tests: nothing defined in '\_subContext', and nothing
defined in testees values. So what's the point ?

### Test files hierarchy

Actually, defining tests in the configuration can quickly increase its 
unreadability level.

Therefore separating context and testee definition from actual tests seems a
good idea when possible. That's why the context hierarchy defined in the
configuration can be used to create a file hierarchy in the configuration
directory: the tests to be run for each context or testee can be stored there
accordingly.

Each context name stripped from its leading underscore correponds to a directory.
If required, testee name can also be used as a directory.
In each directory, tests to be run for this context or this testee are named
using the pattern NN-testname, where NN is a zero-padded number between 00
and 99, and testname a descriptive enough name for the test to be run.

For the above configuration, this leads to the following hierarchy:

---
```
$HOME/.config/zyeute/
├── config.json
├── tests/
│   ├── testA
│   ├── testC
│   ├── test1
│   └── test3
└── context1/
    └── subContext/
        ├── 00-testC => ../../tests/testC
        ├── 01-testD => /usr/bin/testD
        └── subTestee2/
            └── 00-test3 => ../../../tests/test3
```
---


Check this repository [`config/`](config) directory for an actual example.

### Test files

The test files are executables that comply with two constraints:

* handling the relevant arguments sent by Zyeute
* exiting with one of these exit codes:
    * 0 in case of success
    * 1 in case of failure

Apart from these constraints, tests can be anything you wish.

If tests produce any output, it is used as Zyeute warning or recovering messages.

A test failure results in the log of its full context of execution in the `fails`
file located in the same directory as the configuration file.  
A test success occuring after one or more failures of this same test results in
the removal of its full context of execution from the `fails` file.

Test files written in bash can source the output of `zyeute --helpers` in order
to initialize and get access to arguments in the `$options` array.


Check the existing tests in [`config/tests/`](config/tests/) for a better grasp.


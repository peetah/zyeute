#!/bin/bash
source <($(which zyeute) --helpers)

init_options $@

if [ -z ${options["files"]} ]; then
  echo "Missing file name."
  exit 2
else
  file=${options["files"]}
fi

if [ -e  "$file" ]; then
  echo "'$file' is back"
  exit 0
fi

echo "'$file' is missing"
exit 1

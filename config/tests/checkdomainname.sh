#!/bin/bash
source <($(which zyeute) --helpers)

init_options $@

if [ -z "${options["ips"]}" ]; then
  echo "Missing IP."
  exit 2
else
  IP=${options["ips"]}
fi

if [ -z "${options["servernames"]}" ]; then
  echo "Missing server name."
  exit 2
else
  servername=${options["servernames"]}
fi

dig $servername | grep -q $IP
result=$?
if [ $result = 0 ]; then
  echo "'$servername' domain is associated with $IP as expected"
else
  echo "'$servername' domain is not associated with $IP"
fi

exit $result

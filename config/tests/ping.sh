#!/bin/bash
source <($(which zyeute) --helpers)

init_options $@

if [ -z "${options["ips"]}" ]; then
  echo "Missing IP."
  exit 2
else
  IP=${options["ips"]}
fi

PING="ping -n -c 2 -q"
$PING $IP > /dev/null
result=$?
if [ $result = 0 ]; then
  echo "'$IP' is back online"
else
  echo "'$IP' is not alive"
fi
exit $result


#!/bin/bash
source <($(which zyeute) --helpers)

init_options $@

if [ -z "${options["servernames"]}" ]; then
  echo "Missing server name."
  exit 2
else
  servername=${options["servernames"]}
fi

port=443
if [ -n "${options["https_port"]}"  ]; then
  port=${options["https_port"]}
fi

cert=$(echo | openssl s_client -showcerts -servername $servername -connect $servername:$port 2>/dev/null)
echo "$cert"|openssl x509 -inform pem -noout -checkend 1296000 # 15 days
result=$?
if [ $result = 0  ]; then
  echo -n -e "'$servername' domain certificate expires in more than 15 days on "
else
  echo -n -e "'$servername' domain certificate expires in less than 15 days on "
fi
echo "$cert"|openssl x509 -inform pem -noout -enddate|sed -e 's/notAfter=//'
exit $result

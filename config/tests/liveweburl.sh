#!/bin/bash
source <($(which zyeute) --helpers)

init_options $@

if [ -z ${options["protocols"]} ]; then
  echo "Missing protocol."
  exit 2
else
  protocol=${options["protocols"]}
fi

case "$protocol" in
  "http"|"https")
    ;;
  *)
    echo "Protocol "$protocol" not supported."
    exit 2
    ;;
esac

if [ -z ${options["servernames"]} ]; then
  echo "Missing server name."
  exit 2
else
  servername=${options["servernames"]}
fi

case "$protocol" in
  "http")
    port=80
    ;;
  "https")
    port=443
    ;;
esac

if [ -n "${options["${protocol}_port"]}" ]; then
  port=${options["${protocol}_port"]}
fi

if [ -z "${options["URLs"]}" ]; then
  URL=
else
  URL=${options["URLs"]}
  if [ "${URL:0:1}" = "/" ];then
    URL=${URL:1}
  fi
fi

CURL="curl -s -I"
expected="HTTP/.* 200 OK"
URI="${protocol}://${servername}:${port}/${URL}"
response=$($CURL "$URI")
curlExitCode=$?
if [ -z "$response" ];then
  response="curl error: exit code $curlExitCode"
fi
echo -e "$response" | grep "$expected" > /dev/null
result=$?
if [ $result = 0 ];then
  echo -e "'$URI' is fine now"
else
  echo -e "'$URI' returned : \n$response"
fi
exit $result


#!/bin/bash
source <($(which zyeute) --helpers)

init_options $@

if [ -z "${options["servernames"]}" ]; then
  echo "Missing server name."
  exit 2
else
  servername=${options["servernames"]}
fi

port=443
if [ -n "${options["https_port"]}"  ]; then
  port=${options["https_port"]}
fi

cert=$(echo | openssl s_client -showcerts -servername $servername -connect $servername:$port 2>/dev/null)

# WARNING: checkhost option requires openssl >= 1.0.2
echo "$cert"|openssl x509 -inform pem -noout -checkhost $servername|grep -q NOT
if [ $? = 0 ]; then
  result=1
  echo "'$servername' domain certificate does NOT match this servername"
else
  result=0
  echo "'$servername' domain certificate does match this servername"
fi

exit $result
